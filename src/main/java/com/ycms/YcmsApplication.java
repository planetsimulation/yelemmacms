package com.ycms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

import de.codecentric.boot.admin.client.registration.Application;



//@ComponentScan(basePackages="com.planetSimulation.ycms,com.planetSimulation.ycms.controllers,com.planetSimulation.ycms.data,com.planetSimulation.ycms.services")
@SpringBootApplication
public class YcmsApplication{

	public static void main(String[] args) {
		SpringApplication.run(YcmsApplication.class, args);
	}
}