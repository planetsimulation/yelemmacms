package com.ycms.controllers;

import java.util.ArrayList;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.ycms.domains.LabExam;
import com.ycms.domains.LabResult;
import com.ycms.repositories.LabResultRepository;
import com.ycms.security.User;
import com.ycms.services.LabExamService;
import com.ycms.services.LabResultService;
import com.ycms.services.UserService;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Controller
@RequestMapping("/addResult")
@SessionAttributes("labResult")
public class AddResultController {

	
	private UserService userService;
	private LabResultService labResultService;
	private LabExamService labExamService;
	
	@Autowired
	public AddResultController(UserService userService,LabResultService labResultService,LabExamService labExamService) {
		
		this.userService = userService;
		this.labResultService = labResultService;
		this.labExamService=labExamService;
	}
	
	@GetMapping
	public String addResult(@ModelAttribute LabResult labResults) {
		
		
		
		
		return "add_results";
	}
	
	@PostMapping
	public String postResult(@Valid LabResult labResult,BindingResult bindingResult,Model model,SessionStatus sessionStatus) {
		log.info("in post");
		//log.info("inputs---"+labResult.getLabExam().getDoctor().getFirstName()+labResult.getLabExam().getPatient().getUser().getFirstName()+labResult.getData());
		
		
		Optional<LabExam> optionalLabExam= labExamService.findById(labResult.getLabExam().getLabExamId());
		LabExam saveLabExam= optionalLabExam.get();
		saveLabExam.setApproved(1);
		labExamService.save(saveLabExam);
		
		labResultService.save(labResult);
		
		log.info("labresult saved");
		model.addAttribute("successMessage","Result has been posted!");
		sessionStatus.setComplete();
		return "redirect:/viewLab";
	}
}

