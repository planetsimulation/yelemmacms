package com.ycms.controllers;

import java.awt.List;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.ArrayList;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ycms.domains.Appointment;
import com.ycms.repositories.AppointmentRepository;
import com.ycms.services.AppointmentService;

import lombok.extern.slf4j.Slf4j;



@Slf4j
@Controller
@RequestMapping("/approve")
public class AppointmentController{

	
	private final AppointmentService appointmentService;
	
	@Autowired
	public AppointmentController(AppointmentService appointmentService) {
		this.appointmentService= appointmentService;
	}
	
	@ModelAttribute(name = "appointment")
	  public Appointment appointment(Model model) {
	    return new Appointment();
	  }
	
	
	
	@PostMapping
	public String processAppointment(Appointment appointment) {
		log.info("-->"+appointment.toString());
		Optional<Appointment> optionalSaveAppointment=appointmentService.findById(appointment.getAppointmentId());
		Appointment saveAppointment= optionalSaveAppointment.get();
		saveAppointment.setApproved(1);
		appointmentService.save(saveAppointment);
		return "redirect:/approve";
	}
	
	
	
	@GetMapping
	public String showScheduleForm(Model model) {
		ArrayList<Appointment> appointment =new ArrayList<>();
		
		Date appointmentDate= new Date(Calendar.getInstance().getTimeInMillis());
		int approved=0;
		appointmentService.findAppointmentByAppointmentDateAndApproved(appointmentDate, approved)
		.forEach(i->appointment.add(i));

		model.addAttribute("appointment", appointment);
		return "approve";
	}
	
}


