package com.ycms.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ycms.domains.Appointment;
import com.ycms.domains.Patient;
import com.ycms.repositories.AppointmentRepository;
import com.ycms.security.Role;
import com.ycms.security.User;
import com.ycms.services.AppointmentService;
import com.ycms.services.PatientService;
import com.ycms.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/book")
public class BookController{


	private AppointmentService appointmentService;
	
	private UserService userService;
	private PatientService patientService;
	
	
	@Autowired
	public BookController(AppointmentService appointmentService, UserService userService,PatientService patientService) {
		this.appointmentService= appointmentService;
		
		this.userService= userService;
		this.patientService=patientService;
	}
	
	
	
	
	@ModelAttribute(name="user")
	public UserDetails user(@AuthenticationPrincipal UserDetails userDetails ) {
		String username = userDetails.getUsername();
		User user = userService.findUserByUsername(username);
		return user;
		
	}
	
	@ModelAttribute(name = "appointment")
	  public Appointment appointment(Model model) {
	    return new Appointment();
	 }
	 
	
	@GetMapping
	public String showBookForm(@AuthenticationPrincipal UserDetails userDetails,Model model,@ModelAttribute Appointment appointment ) {
		Role doctorRole= new Role();
		doctorRole.setId(2L);
		doctorRole.setRole("DOCTORUSER");
		ArrayList<User> doctors= new ArrayList<>();
		userService.findUserByRoles(doctorRole).forEach(i->doctors.add(i));
		
		model.addAttribute("doctors",doctors);
		User user = userService.findUserByUsername(userDetails.getUsername());
		log.info("user is "+ user.getFirstName());
		
		
		
		
		
		return "book";
	}
	
	@PostMapping
	public String processBook(@Valid Appointment appointment,@AuthenticationPrincipal UserDetails userDetails, Model model) {
		
		User user = userService.findUserByUsername(userDetails.getUsername());
		log.info("user is "+ user.getFirstName());
		Patient patient= patientService.findPatientByUser(user);
		
		appointment.setPatient(patient);
		
		
		
		log.info("Date is "+appointment.getAppointmentDate().toString());
		//log.info("patient is"+appointment.getPatient().toString());
		log.info("doctor id is "+appointment.getDoctor().toString());
		log.info("Appointment time is "+appointment.getAppointmentTime().toString());
		
		
	
		Appointment appointmentExists= appointmentService.findAppointmentByAppointmentDateAndAppointmentTimeAndDoctor(appointment.getAppointmentDate(),appointment.getAppointmentTime(),appointment.getDoctor());
		log.info("After appointment exist");
		if(appointmentExists!=null) {
			log.info("inside first if");
			model.addAttribute("errorMessage","Already booked");
			return "book";
			
		}
		
		
		else {
			appointmentService.save(appointment);
			model.addAttribute("successMessage", "Appointment has been booked");
			return "book";
		}
		
		
	}
	
	
	
	
	
}

