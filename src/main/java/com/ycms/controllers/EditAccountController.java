package com.ycms.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ycms.security.User;
import com.ycms.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class EditAccountController {
	
	private UserService userService;
	
	@Autowired
	public EditAccountController(UserService userService) {
		// TODO Auto-generated constructor stub
		this.userService = userService;
	}
	
	@GetMapping("/editAccount")
	public String EditAccount(@AuthenticationPrincipal UserDetails userDetails,Model model) {
		String username = userDetails.getUsername();
		User user = userService.findUserByUsername(username);
		log.debug("-------->"+user.getFirstName());
		model.addAttribute("user", user);
		return "edit_account";
	}
	
	@PostMapping("/editAccount")
	public String Update() {
		
		return "edit_account";
	}

}
