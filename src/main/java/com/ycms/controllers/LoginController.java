package com.ycms.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller

public class LoginController {
	
	
	
	
	@GetMapping("/default")
	public String LoadDefaultLanding(@AuthenticationPrincipal UserDetails userDetails) {
		
	
		
		if(userDetails.getAuthorities().toString().equals("[PATIENTUSER]")) {
			return "redirect:/book";
		}
		
		
		else if(userDetails.getAuthorities().toString().equals("[DOCTORUSER]")) {
			return "redirect:/schedule";
		}
		else if(userDetails.getAuthorities().toString().equals("[RECEPTIONISTUSER]")) {
			return "redirect:/approve";
		}
		else if(userDetails.getAuthorities().toString().equals("[LABTECHUSER]")) {
			return "redirect:/viewLab";
		}
		
		return "redirect:/approve";
	}
	
	@GetMapping("/login")
	public String login() {
		return "login";
	}
	
	@GetMapping("/access-denied")
    public String accessDenied(){
        return "access_denied";
    }

	
	
}

