package com.ycms.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.ycms.domains.LabExam;
import com.ycms.services.AppointmentService;
import com.ycms.services.LabExamService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/orderLab")
@SessionAttributes("labExam")
public class OrderLabController {
	
	private LabExamService labExamService;
	private AppointmentService appointmentService;
	
	@Autowired
	public OrderLabController(LabExamService labExamService) {
		this.labExamService=labExamService;
		this.appointmentService=appointmentService;
	}
	
	@GetMapping
	public String orderLabForm(@ModelAttribute LabExam labExam) {
		
		log.info("passed lab exam is "+ labExam.toString() );
		return "order_lab";
		
	}
	
	@PostMapping
	public String processLabExam(LabExam labExam, Model model,SessionStatus sessionStatus) {
		log.info("processed lab exam is"+labExam.toString());
		
		labExamService.save(labExam);
		model.addAttribute("successMessage","Lab exam has been ordered");
		sessionStatus.setComplete();
		return "redirect:/schedule";
	}

	
}
