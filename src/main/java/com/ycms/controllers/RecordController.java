package com.ycms.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ycms.domains.Appointment;
import com.ycms.domains.Record;
import com.ycms.repositories.RecordRepository;
import com.ycms.security.User;
import com.ycms.services.RecordService;
import com.ycms.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/viewRecord")
public class RecordController {
	private RecordService recordService;
	private UserService userService;
	
	@Autowired
	public RecordController(RecordService recordService, UserService userService) {
		this.recordService= recordService;
		this.userService= userService;
		
	}
	
	@ModelAttribute(name="user")
	public UserDetails user(@AuthenticationPrincipal UserDetails userDetails ) {
		String username = userDetails.getUsername();
		User user = userService.findUserByUsername(username);
		return user;
		
	}
	
	@ModelAttribute(name = "record")
	public Record record(Model model) {
	    return new Record();
	}
	
	@GetMapping
	public String showRecords(@AuthenticationPrincipal UserDetails userDetails, Model model) {
		
		String username= userDetails.getUsername();
		User user= userService.findUserByUsername(username);
		
	
		ArrayList<Record> recordList = new ArrayList<>();

		log.info("User id is "+user.getId());
		recordList=recordService.findRecordByDoctorId(user.getId());
		
		log.info("record array is"+ recordList.toString());

		
		model.addAttribute("record", recordList);
		
		return "viewRecord";
	}
}
