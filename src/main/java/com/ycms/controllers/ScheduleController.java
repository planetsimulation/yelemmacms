package com.ycms.controllers;



import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.ycms.domains.Appointment;
import com.ycms.domains.LabExam;
import com.ycms.repositories.AppointmentRepository;
import com.ycms.repositories.UserRepository;
import com.ycms.security.Role;
import com.ycms.security.User;
import com.ycms.services.AppointmentService;
import com.ycms.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/schedule")
@SessionAttributes("labExam")

public class ScheduleController {
	
	private AppointmentService appointmentService;

	private UserService userService;
	
	@Autowired
	public ScheduleController(AppointmentService appointmentService, UserService userService) {
		this.appointmentService= appointmentService;
		this.userService= userService;

	}
	
	@ModelAttribute(name="user")
	public UserDetails user(@AuthenticationPrincipal UserDetails userDetails ) {
		String username = userDetails.getUsername();
		User user = userService.findUserByUsername(username);
		return user;
		
	}
	
	@ModelAttribute(name = "appointment")
	  public Appointment appointment(Model model) {
	    return new Appointment();
	 }
	
	@ModelAttribute(name="labExam")
	public LabExam labExam(Model model) {
		return new LabExam();
	}
	
	@GetMapping
	public String showDailyScheduleForm(@AuthenticationPrincipal UserDetails userDetails,Model model) {
		String username= userDetails.getUsername();
		User user= userService.findUserByUsername(username);
		
		
		
		ArrayList<Appointment> appointment = new ArrayList<>();
		
		
		//ArrayList<String> patientNames= new ArrayList();
		
		 //DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		 Date appointmentDate = new Date(Calendar.getInstance().getTimeInMillis());
		 
		 Optional<User> doctor =userService.findById(user.getId()) ;
		 int approved =1;
		 
		 log.info("today is "+ appointmentDate.toString());
		 log.info("usr id is "+ user.getId());
	
		//appointmentService.findAll().forEach(i->appointment.add(i));
		appointmentService.findAppointmentByAppointmentDateAndDoctorAndApproved(appointmentDate,doctor.get(), approved)
		.forEach(i->appointment.add(i));
		
		Map<Appointment,User> m1 = new HashMap(); 
		
		/*for(Appointment app: appointment) {
			Optional<User> u= userService.findById(app.getPatientId());
			
			String patientName;
			patientName=u.get().getFirstName();
			
			m1.put(app, u.get());
			
		}*/
		
		model.addAttribute("appointmentMap", appointment);
		return "schedule";
	}
	
	@PostMapping
	public String processAppointment(Appointment appointment, LabExam labExam ) {

		log.info("Appointment being passed"+appointment.toString());
		Optional<Appointment> passAppointment= appointmentService.findById(appointment.getAppointmentId());
		labExam.setPatient(passAppointment.get().getPatient());
		labExam.setDoctor(passAppointment.get().getDoctor());
		
		log.info("LabExam being passed"+labExam.toString());
	
		return "redirect:/orderLab";
	}
	

}
