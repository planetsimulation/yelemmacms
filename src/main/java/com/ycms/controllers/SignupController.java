package com.ycms.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.ycms.domains.Patient;
import com.ycms.domains.PatientRegistrationForm;
import com.ycms.security.User;
import com.ycms.services.PatientService;
import com.ycms.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class SignupController {

	
	private UserService userService;
	private PatientService patientService;
	
	@Autowired
	public SignupController(UserService userService, PatientService patientService) {
		this.userService=userService;
		this.patientService=patientService;
	}
	
	@ModelAttribute(name="patientRegistrationForm")
	public PatientRegistrationForm patientRegistrationForm(Model model) {	
		return new PatientRegistrationForm();
	}
	
	@GetMapping("/signup")
	public String signup() {
		return "signup";
	}
	
	@PostMapping("/signup")
    public String createNewUser(@Valid PatientRegistrationForm patientRegistrationForm, BindingResult bindingResult, Model model) {
        User userExists = userService.findUserByUsername(patientRegistrationForm.getUsername());
        
        Iterable<Patient> patients=patientService.findAll();

        if (userExists != null) {
        	 model.addAttribute("errorMessage","Username already exists");
        	 return "signup";
            
        }
        else {
        	
            userService.saveUser(patientRegistrationForm.toUser());
            User addedUser= userService.findUserByUsername(patientRegistrationForm.getUsername());
            Patient newPatient= patientRegistrationForm.toPatient();
            newPatient.setUser(addedUser);
            patientService.savePatient(newPatient);
            
            model.addAttribute("successMessage", "User has been registered successfully");
            
            return "redirect:/login";
        }
    }
}

