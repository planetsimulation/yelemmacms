package com.ycms.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.ycms.domains.LabExam;
import com.ycms.domains.LabResult;
import com.ycms.domains.Record;
import com.ycms.security.User;
import com.ycms.services.LabExamService;
import com.ycms.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/viewLab")
@SessionAttributes("labResult")
public class ViewLabController {
	
	private LabExamService labExamService;
	private UserService userService;
	
	@Autowired
	public ViewLabController(LabExamService labExamService, UserService userService) {
		this.labExamService= labExamService;
		this.userService= userService;
	}
	
	@ModelAttribute(name="labExam")
	public LabExam labExam(Model model) {
		return new LabExam();
	}
	
	@ModelAttribute(name="labResult")
	public LabResult labResult(Model model) {
		return new LabResult();
	}
	
	@GetMapping
	public String ShowLabExams(Model model) {
		
		log.info("insideeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
		ArrayList<LabExam> labExams= new ArrayList<LabExam>();
		
		labExamService.findLabExamByApproved(0).forEach(i->labExams.add(i));

		
		Map<LabExam,List<User>> labExamMap=new HashMap();
		
		
		/*for(LabExam labExam: labExams) {
			Optional<User> patient= userService.findById(labExam.getPatientId());
			Optional<User> doctor= userService.findById(labExam.getDoctorId());
			
			List<User> userList=new ArrayList<User>();
			
			userList.add(patient.get());
			userList.add(doctor.get());
			
			labExamMap.put(labExam, userList);
			
		}*/
		model.addAttribute("labExamMap", labExams);
		
		return "view_lab";
		
	}
	
	@PostMapping
	public String processLabExam(LabResult labResult, LabExam labExam) {
		Optional<LabExam> passLabExam=labExamService.findById(labExam.getLabExamId());
		
		
		
		//= new LabResult();
		labResult.setLabExam(passLabExam.get());
		
		
		return "redirect:/addResult";
	}

}