package com.ycms.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.ycms.domains.LabExam;
import com.ycms.domains.LabResult;
import com.ycms.repositories.LabResultRepository;
import com.ycms.security.User;
import com.ycms.services.LabExamService;
import com.ycms.services.LabResultService;
import com.ycms.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@SessionAttributes("labResult")
public class ViewLabResultsController {
	
	
	private LabResultService labResultService;
	private UserService userService;
	private LabExamService labExamService;
	
	@Autowired
	public ViewLabResultsController(LabResultService labResultService,UserService userService,LabExamService labExamService) {
		// TODO Auto-generated constructor stub
	
		this.labResultService = labResultService;
		this.userService = userService;
		this.labExamService= labExamService;
	}

	@ModelAttribute(name="labResult")
	public LabResult labResult(Model model) {
		return new LabResult();
	}
	
	
	
	@GetMapping("/viewLabResults")
	public String ViewLabResults(@AuthenticationPrincipal UserDetails userDetails,Model model) {
		log.info("in get");
		
		ArrayList<LabResult> labResults = new ArrayList<LabResult>();
		
		log.info("---------");
		String username = userDetails.getUsername();
		User user = userService.findUserByUsername(username);
		log.info("---------"+user.toString());

		ArrayList<LabExam> labExam= labExamService.findLabExamByApprovedAndDoctor(1,user);
		log.info("---------1"+labExam.size());
		
//		log.info(msg);
		for(LabExam le:labExam) {
			LabResult lr = labResultService.findLabResultsByLabExamAndSeen(le,0);
			if(lr != null) labResults.add(lr);
		}
		
		
		
		log.info("------"+labResults.size());
	
		model.addAttribute("labResult",labResults);
		
		return "viewLabResults";
	}
	
	@PostMapping("/viewLabResults")
	public String ResultIsSeen(LabResult labResult) {
		log.info("in post");
		//log.info("----"+labResult);
//		Optional<LabResult> labResultID=labResultService.findLabResultsByResultId(labResult.getResultId());
//		log.info("----"+labResultID);
//		LabResult seen = labResultID.get();
//		log.info("----"+seen);
//		seen.setSeen(1);
//		log.info("----"+labResult.getSeen());
		labResult.setSeen(1);
		
		labResultService.save(labResult);
		log.info("get seen-->"+labResult.getResultId());
		//log.info("lab result--->"+labResult);
		return "redirect:/viewLabResults";
	}
}
