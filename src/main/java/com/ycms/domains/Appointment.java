package com.ycms.domains;



import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import com.ycms.security.User;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;



@Data
@Entity
@Table(name="appointment")
public class Appointment {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long appointmentId;
	
	
	
	  
	@DateTimeFormat(pattern="HH:mm")
	@Temporal(value = TemporalType.TIME )
	@Column(name="appointment_time")
	private Date appointmentTime;
	  
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(value = TemporalType.DATE )
	@Column(name="appointment_date")
	private Date appointmentDate;
	
	@Column(name="approved")
	private int approved;
	
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER,targetEntity=User.class)
	private User doctor;
	
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER,targetEntity=Patient.class)
	private Patient patient;

}

