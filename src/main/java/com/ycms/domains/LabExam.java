package com.ycms.domains;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.ycms.security.User;

import lombok.Data;

@Data
@Entity
@Table(name="lab_exam")
public class LabExam {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long labExamId;
	
	@Column(name="test_type")
	@NotBlank(message="please fill in the data")
	private String testType;
	
	@Column(name="approved")
	private int approved;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "doctor_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User doctor;
	

	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "patient_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
	private Patient patient;
	
	
	 @OneToOne(fetch = FetchType.LAZY, cascade =  CascadeType.ALL,mappedBy = "labExam")
	 private LabResult labResult;
	


}

