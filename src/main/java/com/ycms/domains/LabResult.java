package com.ycms.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
@Entity
@Table(name="result")
public class LabResult {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long resultId;
	
	
	@Column(name="data")
	@NotBlank(message="please fill in the data")
	private String data;
	
	@Column(name="seen")
	private int seen;
	
	@OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "labexam_id", nullable = false)
    private LabExam labExam;
}
