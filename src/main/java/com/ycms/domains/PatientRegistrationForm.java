package com.ycms.domains;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.ycms.security.User;

import lombok.Data;

@Data
public class PatientRegistrationForm {

	@NotBlank(message="Please provide a username")
	private String username;
	
	@Size(min = 5, message = "Your password must have at least 5 characters")
    @NotBlank(message = "Please provide your password")
	private String password;
	
	@Column(name = "user_fname")
    @NotBlank(message = "Please provide your first name")
    private String firstName;
	
	@Column(name = "user_lname")
    @NotBlank(message = "Please provide your last name")
    private String lastName;
	
	@Column(name="email")
	@NotBlank(message="Email is required")
	private String email;
	
	@NotNull(message="Please enter your age")
	@Column(name="age")
	private int age;
	
	@NotBlank(message="Must be filled")
	@Column(name="sex")
	private String sex;
	
	@NotBlank(message="Please enter your phone number")
	@Column(name="phone_number")
	private String phoneNo;
	
	public User toUser() {
		return new User(username,password,firstName,lastName,email);
	}
	
	public Patient toPatient() {
		return new Patient(age,sex,phoneNo);
		}
	
	
	
}
