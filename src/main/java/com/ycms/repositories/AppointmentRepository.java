package com.ycms.repositories;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.ycms.domains.Appointment;
import com.ycms.security.User;

public interface AppointmentRepository extends CrudRepository<Appointment, Long> {
	
	public Iterable<Appointment> findAllByAppointmentDate(Date appointmentDate);
	
	public Iterable<Appointment> findAllByDoctor(User doctorId);
	Appointment findAppointmentByAppointmentDateAndAppointmentTimeAndDoctor(Date appointmentDate,Date appointmentTime,User doctorId );
	
	public ArrayList<Appointment> findAppointmentByAppointmentDateAndDoctorAndApproved(Date appointmentDate,User doctorId, int approved);
	
	public ArrayList<Appointment> findAppointmentByAppointmentDateAndApproved(Date appointmentDate, int approved);

	
	
}

