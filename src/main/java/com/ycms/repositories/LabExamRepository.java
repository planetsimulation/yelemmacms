package com.ycms.repositories;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

import com.ycms.domains.LabExam;
import com.ycms.security.User;

public interface LabExamRepository extends CrudRepository<LabExam, Long> {

	ArrayList<LabExam> findLabExamByApproved(int approved);
	
	ArrayList<LabExam> findLabExamByApprovedAndDoctor(int approved,User doctor);
}
