package com.ycms.repositories;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.ycms.domains.LabExam;
import com.ycms.domains.LabResult;
import com.ycms.security.User;

public interface LabResultRepository extends CrudRepository<LabResult, Long>{
	//public Iterable<LabResult> findAllByResultID(long resultID);


	public Optional<LabResult> findLabResultsByResultId(long resultId);
	
	
	
	public Optional<LabResult> findLabResultsByLabExam(LabExam labExam);
	
	public LabResult findLabResultsByLabExamAndSeen(LabExam labExam,int seen);

	public ArrayList<LabResult> findLabResultsBySeen(int seen);

	//public void remove(LabResult labResult);
}
