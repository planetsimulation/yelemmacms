package com.ycms.repositories;

import org.springframework.data.repository.CrudRepository;

import com.ycms.domains.Patient;
import com.ycms.security.User;

public interface PatientRepository  extends CrudRepository<Patient, Long>{

	Patient findPatientByUser(User user);
	Patient findPatientById(Long id);
}
