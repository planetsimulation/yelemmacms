package com.ycms.repositories;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

import com.ycms.domains.Record;

public interface RecordRepository extends CrudRepository<Record, Long>{
	public ArrayList<Record> findAllByPatientId(long patientId);
	
	public ArrayList<Record> findRecordByDoctorId(long doctorId);
}
