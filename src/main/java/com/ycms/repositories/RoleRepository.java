package com.ycms.repositories;

import org.springframework.data.repository.CrudRepository;

import com.ycms.security.Role;



public interface RoleRepository extends CrudRepository<Role, Long>{

	 Role findByRole(String role);
	 
}
