package com.ycms.repositories;

import java.util.ArrayList;



import org.springframework.data.repository.CrudRepository;

import com.ycms.security.Role;
import com.ycms.security.User;



public interface UserRepository extends CrudRepository<User, Long>{
	
	User findByUsername(String username);
	
	ArrayList<User> findUserByRoles(Role role);
	
}
