package com.ycms.security;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.ycms.domains.Patient;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@Entity
@Table(name="user_data")
public class User implements UserDetails{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank(message="Please provide a username")
	private String username;
	
	@Size(min = 5, message = "Your password must have at least 5 characters")
    @NotBlank(message = "Please provide your password")
	private String password;
	
	@Column(name = "user_fname")
    @NotBlank(message = "Please provide your first name")
    private String firstName;
	
	@Column(name = "user_lname")
    @NotBlank(message = "Please provide your last name")
    private String lastName;
	
	@Column(name="email")
	@NotBlank(message="Email is required")
	private String email;
	

	
	
	@ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinTable(name="user_role",
				joinColumns= {@JoinColumn(name="user_id")},
				inverseJoinColumns= {@JoinColumn(name="role_id")})
	private Set<Role> roles;
	

	

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		Set<GrantedAuthority> authorities= roles
				.stream()
				.map(role->new SimpleGrantedAuthority(role.getRole()))
				.collect(Collectors.toSet());
		return authorities;
	}


	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}


	public User(String username2,
			String password2, String firstName2,
			String lastName2,
			String email2) {
		// TODO Auto-generated constructor stub
		username=username2;
		password=password2;
		firstName=firstName2;
		lastName=lastName2;
		email=email2;
		
	}
	
	public User() {
		
	}
	



	
	
	
	

}
