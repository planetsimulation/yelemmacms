package com.ycms.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import com.ycms.domains.Appointment;
import com.ycms.security.User;

public interface AppointmentService {
	
	public Appointment save(Appointment appointment);
	
	public Iterable<Appointment> saveAll(Iterable<Appointment> appointments);
		
	Optional<Appointment> findById(Long id);
	
	boolean existsById(Long id);
	
	Iterable<Appointment> findAll();
	
	Iterable<Appointment> findAllById(Iterable<Long> ids);
	
	long count();
	
	void deleteById(Long id);
	
	void delete(Appointment appointment);
	
	void deletAll(Iterable<Appointment> appointment);
	
	void deletAll();
	
	public Iterable<Appointment> findAllByAppointmentDate(Date appointmentDate);
	
	public Iterable<Appointment> findAllByDoctor(User doctorId);
	Appointment findAppointmentByAppointmentDateAndAppointmentTimeAndDoctor(Date appointmentDate,Date appointmentTime,User doctorId );
	
	public ArrayList<Appointment> findAppointmentByAppointmentDateAndDoctorAndApproved(Date appointmentDate,User doctorId, int approved);
	
	public ArrayList<Appointment> findAppointmentByAppointmentDateAndApproved(Date appointmentDate, int approved);
	
	
	

}
