package com.ycms.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.ycms.domains.Appointment;
import com.ycms.repositories.AppointmentRepository;
import com.ycms.security.User;

@Service
public class AppointmentServiceImpl implements AppointmentService {
	
	AppointmentRepository appointmentRepository;

		public AppointmentServiceImpl(AppointmentRepository appointmentRepository) {
			this.appointmentRepository= appointmentRepository;
		}

		@Override
		public Appointment save(Appointment appointment) {
			// TODO Auto-generated method stub
			return appointmentRepository.save(appointment);
		}

		@Override
		public Iterable<Appointment> saveAll(Iterable<Appointment> appointments) {
			// TODO Auto-generated method stub
			return appointmentRepository.saveAll(appointments);
		}

		@Override
		public Optional<Appointment> findById(Long id) {
			// TODO Auto-generated method stub
			return appointmentRepository.findById(id);
		}

		@Override
		public boolean existsById(Long id) {
			// TODO Auto-generated method stub
			return appointmentRepository.existsById(id);
		}

		@Override
		public Iterable<Appointment> findAll() {
			// TODO Auto-generated method stub
			return appointmentRepository.findAll();
		}

		@Override
		public Iterable<Appointment> findAllById(Iterable<Long> ids) {
			// TODO Auto-generated method stub
			return appointmentRepository.findAllById(ids);
		}

		@Override
		public long count() {
			// TODO Auto-generated method stub
			return appointmentRepository.count();
		}

		@Override
		public void deleteById(Long id) {
			// TODO Auto-generated method stub
			appointmentRepository.deleteById(id);
		}

		@Override
		public void delete(Appointment appointment) {
			// TODO Auto-generated method stub
			appointmentRepository.delete(appointment);
		}

		@Override
		public void deletAll(Iterable<Appointment> appointment) {
			// TODO Auto-generated method stub
			appointmentRepository.deleteAll(appointment);
		}

		@Override
		public void deletAll() {
			// TODO Auto-generated method stub
			appointmentRepository.deleteAll();
		}

		@Override
		public Iterable<Appointment> findAllByAppointmentDate(Date appointmentDate) {
			// TODO Auto-generated method stub
			return appointmentRepository.findAllByAppointmentDate(appointmentDate);
		}

		@Override
		public Iterable<Appointment> findAllByDoctor(User doctorId) {
			// TODO Auto-generated method stub
			return appointmentRepository.findAllByDoctor(doctorId);
		}

		@Override
		public Appointment findAppointmentByAppointmentDateAndAppointmentTimeAndDoctor(Date appointmentDate,
				Date appointmentTime, User doctorId) {
			// TODO Auto-generated method stub
			return appointmentRepository.findAppointmentByAppointmentDateAndAppointmentTimeAndDoctor(appointmentDate, appointmentTime, doctorId);
		}

		@Override
		public ArrayList<Appointment> findAppointmentByAppointmentDateAndDoctorAndApproved(Date appointmentDate,
				User doctorId, int approved) {
			// TODO Auto-generated method stub
			return appointmentRepository.findAppointmentByAppointmentDateAndDoctorAndApproved(appointmentDate, doctorId, approved);
		}

		@Override
		public ArrayList<Appointment> findAppointmentByAppointmentDateAndApproved(Date appointmentDate, int approved) {
			// TODO Auto-generated method stub
			return appointmentRepository.findAppointmentByAppointmentDateAndApproved(appointmentDate, approved);
		}

		
}
