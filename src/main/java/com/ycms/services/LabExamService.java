package com.ycms.services;

import java.util.ArrayList;
import java.util.Optional;

import com.ycms.domains.LabExam;
import com.ycms.security.User;

public interface LabExamService {

	public LabExam save(LabExam labExam);
	
	Optional<LabExam> findById(Long id);
	
	Iterable<LabExam> findAll();
	
	ArrayList<LabExam> findLabExamByApproved(int approved);
	
	ArrayList<LabExam> findLabExamByApprovedAndDoctor(int approved,User doctor);
	
}
