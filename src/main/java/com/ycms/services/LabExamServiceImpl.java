package com.ycms.services;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.ycms.domains.LabExam;
import com.ycms.repositories.LabExamRepository;
import com.ycms.security.User;

@Service
public class LabExamServiceImpl implements LabExamService {
	
	LabExamRepository labExamRepository;
	
	public LabExamServiceImpl(LabExamRepository labExamRepository) {
		this.labExamRepository=labExamRepository;
	}

	@Override
	public LabExam save(LabExam labExam) {
		// TODO Auto-generated method stub
		return labExamRepository.save(labExam);
	}

	@Override
	public Optional<LabExam> findById(Long id) {
		// TODO Auto-generated method stub
		return labExamRepository.findById(id);
	}

	@Override
	public ArrayList<LabExam> findLabExamByApproved(int approved) {
		// TODO Auto-generated method stub
		return labExamRepository.findLabExamByApproved(approved);
	}

	@Override
	public ArrayList<LabExam> findLabExamByApprovedAndDoctor(int approved,User doctor) {
		// TODO Auto-generated method stub
		return labExamRepository.findLabExamByApprovedAndDoctor(approved,doctor);
	}

	@Override
	public Iterable<LabExam> findAll() {
		// TODO Auto-generated method stub
		return labExamRepository.findAll();
	}
	
	

}
