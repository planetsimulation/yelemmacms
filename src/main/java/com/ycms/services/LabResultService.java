package com.ycms.services;

import java.util.ArrayList;
import java.util.Optional;

import com.ycms.domains.LabExam;
import com.ycms.domains.LabResult;
import com.ycms.security.User;

public interface LabResultService {
	public LabResult save(LabResult labResult);
	Optional<LabResult> findLabResultsByResultId(long resultId);
	Optional<LabResult> findLabResultsByLabExam(LabExam labExam);
	public LabResult findLabResultsByLabExamAndSeen(LabExam labExam,int seen);
	ArrayList<LabResult> findLabResultsBySeen(int seen);
	public Iterable<LabResult> findAll();
	//public void remove(LabResult labResult);
}
