package com.ycms.services;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.ycms.domains.LabExam;
import com.ycms.domains.LabResult;
import com.ycms.repositories.LabResultRepository;
import com.ycms.security.User;

@Service
public class LabResultServiceImpl implements LabResultService{

	LabResultRepository labResultRepo;
	
	public LabResultServiceImpl(LabResultRepository labResultRepository) {
		this.labResultRepo = labResultRepository;
	}
	
	@Override
	public LabResult save(LabResult labResult) {
		return labResultRepo.save(labResult);
	}

	@Override
	public Optional<LabResult> findLabResultsByResultId(long resultId) {
		// TODO Auto-generated method stub
		return labResultRepo.findLabResultsByResultId(resultId);
	}

	@Override
	public ArrayList<LabResult> findLabResultsBySeen(int seen) {
		// TODO Auto-generated method stub
		return labResultRepo.findLabResultsBySeen(seen);
	}

	@Override
	public Optional<LabResult> findLabResultsByLabExam(LabExam labExam) {
		// TODO Auto-generated method stub
		return labResultRepo.findLabResultsByLabExam(labExam);
	}

	@Override
	public LabResult findLabResultsByLabExamAndSeen(LabExam labExam,int seen) {
		// TODO Auto-generated method stub
		return labResultRepo.findLabResultsByLabExamAndSeen( labExam,seen);
	}

	@Override
	public Iterable<LabResult> findAll() {
		// TODO Auto-generated method stub
		return labResultRepo.findAll();
	}

//	@Override
//	public void remove(LabResult labResult) {
//		// TODO Auto-generated method stub
//		return;
//	}
}
