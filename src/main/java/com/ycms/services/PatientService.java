package com.ycms.services;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.ycms.domains.Patient;
import com.ycms.security.User;


public interface PatientService {

	void savePatient(Patient patient);
	
	Patient findPatientByUser(User user);
	Optional<Patient> findPatientById(Long id);
	
	Iterable<Patient> findAll();
}
