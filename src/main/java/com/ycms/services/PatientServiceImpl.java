package com.ycms.services;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ycms.domains.Patient;
import com.ycms.repositories.PatientRepository;
import com.ycms.security.User;

@Service
public class PatientServiceImpl implements PatientService {
	
	private PatientRepository patientRepository;
	
	@Autowired
	public PatientServiceImpl(PatientRepository patientRepository) {
		this.patientRepository= patientRepository;
	}

	@Override
	public void savePatient(Patient patient) {
		patientRepository.save(patient);
		
	}

	@Override
	public Patient findPatientByUser(User user) {
		return patientRepository.findPatientByUser(user);
	}

	@Override
	public Optional<Patient> findPatientById(Long id) {
	
		return patientRepository.findById(id);
	}

	@Override
	public Iterable<Patient> findAll() {
		// TODO Auto-generated method stub
		return patientRepository.findAll();
	}

}
