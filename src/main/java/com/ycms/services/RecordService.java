package com.ycms.services;



import java.util.ArrayList;

import com.ycms.domains.Record;


public interface RecordService {
	public void saveRecord(Record record);
	
	public ArrayList<Record> findRecordByDoctorId(long doctorId);
}