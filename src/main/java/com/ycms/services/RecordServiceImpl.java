package com.ycms.services;


import com.ycms.domains.Record;
import com.ycms.repositories.RecordRepository;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class RecordServiceImpl implements RecordService{
	private RecordRepository recordRepository;
    
	@Autowired
    public RecordServiceImpl(RecordRepository recordRepository) {
		this.recordRepository = recordRepository;
	}
	
    public void saveRecord(Record record) {
        recordRepository.save(record);
    }

	@Override
	public ArrayList<Record> findRecordByDoctorId(long doctorId) {
		return recordRepository.findRecordByDoctorId(doctorId);
		
	}
}