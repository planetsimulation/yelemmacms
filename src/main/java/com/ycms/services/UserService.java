package com.ycms.services;

import java.util.ArrayList;
import java.util.Optional;



import org.springframework.security.core.userdetails.UserDetailsService;

import com.ycms.security.Role;
import com.ycms.security.User;



public interface UserService extends UserDetailsService {

	User findUserByUsername(String username);
	void saveUser(User user);
	
	Optional<User> findById(long id);
	ArrayList<User> findUserByRoles(Role role);
	
}
