package com.planetSimulation.ycms;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.ycms.controllers.LoginController;

import lombok.experimental.Wither;

@RunWith(SpringRunner.class)
@WebMvcTest(value = LoginController.class, secure = false)
@ContextConfiguration()
public class LoginControllerTest {

	private int port = 8080;
	
	@Autowired
	private MockMvc mockMvc;

	private static HtmlUnitDriver browser;
	
	@BeforeClass
	public static void setup() {
			browser = new HtmlUnitDriver();
			browser.manage().timeouts()
			    .implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	  @AfterClass
	  public static void closeBrowser() {
		  browser.quit();
	  }

	@Test
	public void test() throws Exception {
		this.mockMvc.perform(get("/login"))
				.andExpect(status().isOk())
				.andExpect(view().name("login"))
				.andReturn();
	}
	
	@Test
	public void testLoginAttempt() throws Exception{
		browser.get(homePageUrl());
		doLogin("aj","password");
	}

	private void doLogin(String username, String password) {
	    browser.findElementByName("username").sendKeys(username);
	    browser.findElementByName("password").sendKeys(password);
	    browser.findElementByCssSelector("form#loginForm").submit();
	}
	
	private String homePageUrl() {
	    return "http://localhost:" + port + "/login";
	}
}
