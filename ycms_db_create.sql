create table appointment ( 
    appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB;

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB;

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB;

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB;

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB;
    
create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB;

    create table user_role (
       user_id bigint not null,
        role_id bigint not null
    ) engine=InnoDB;


       
    ALTER TABLE `appointment`

  ADD KEY `FK_DoctorAppointment` (`doctor_id`),
  ADD KEY `FK_PatientAppointment` (`patient_id`);

ALTER TABLE `lab_exam`

  ADD KEY `FK_PatientLab` (`patient_id`),
  ADD KEY `FK_DoctorLab` (`doctor_id`);


ALTER TABLE `record`

  ADD UNIQUE KEY `record_id` (`record_id`),
  ADD KEY `FK_PatientRecord` (`patient_id`),
  ADD KEY `FK_DoctorRecord` (`doctor_id`);

ALTER TABLE `result`

  ADD UNIQUE KEY `result_id` (`result_id`),
  ADD KEY `FK_PatientResult` (`patient_id`),
  ADD KEY `FK_DoctorResult` (`doctor_id`);



ALTER TABLE `user_data`

  ADD UNIQUE KEY `user_id` (`id`);



ALTER TABLE `appointment`
  MODIFY `appointment_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

ALTER TABLE `lab_exam`
  MODIFY `lab_exam_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

ALTER TABLE `record`
  MODIFY `record_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `result`
  MODIFY `result_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `role`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

ALTER TABLE `user_data`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

ALTER TABLE `appointment`
  ADD CONSTRAINT `FK_DoctorAppointment` FOREIGN KEY (`doctor_id`) REFERENCES `user_data` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PatientAppointment` FOREIGN KEY (`patient_id`) REFERENCES `user_data` (`id`) ON UPDATE CASCADE;

ALTER TABLE `lab_exam`
  ADD CONSTRAINT `FK_DoctorLab` FOREIGN KEY (`doctor_id`) REFERENCES `user_data` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PatientLab` FOREIGN KEY (`patient_id`) REFERENCES `user_data` (`id`) ON UPDATE CASCADE;

ALTER TABLE `record`
  ADD CONSTRAINT `FK_DoctorRecord` FOREIGN KEY (`doctor_id`) REFERENCES `user_data` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PatientRecord` FOREIGN KEY (`patient_id`) REFERENCES `user_data` (`id`) ON UPDATE CASCADE;

ALTER TABLE `result`
  ADD CONSTRAINT `FK_DoctorResult` FOREIGN KEY (`doctor_id`) REFERENCES `user_data` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PatientResult` FOREIGN KEY (`patient_id`) REFERENCES `user_data` (`id`) ON UPDATE CASCADE;
    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint not null,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        patient_id bigint,
        doctor_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint,
        patient_id bigint,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint,
        patient_id bigint,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint,
        patient_id bigint,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint,
        patient_id bigint,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=MyISAM

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=MyISAM

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=MyISAM

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=MyISAM

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=MyISAM

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=MyISAM

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id)

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=MyISAM

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=MyISAM

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=MyISAM

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=MyISAM

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=MyISAM

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=MyISAM

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id)

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=MyISAM

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=MyISAM

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=MyISAM

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=MyISAM

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=MyISAM

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=MyISAM

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id)

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=MyISAM

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=MyISAM

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=MyISAM

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=MyISAM

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=MyISAM

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=MyISAM

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id)

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=MyISAM

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=MyISAM

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=MyISAM

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=MyISAM

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=MyISAM

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=MyISAM

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id)

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=MyISAM

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=MyISAM

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=MyISAM

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=MyISAM

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=MyISAM

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=MyISAM

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id)

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=MyISAM

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=MyISAM

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=MyISAM

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=MyISAM

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=MyISAM

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=MyISAM

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id)

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=MyISAM

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=MyISAM

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=MyISAM

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=MyISAM

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=MyISAM

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=MyISAM

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id)

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=MyISAM

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=MyISAM

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=MyISAM

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=MyISAM

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=MyISAM

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=MyISAM

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id)

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=MyISAM

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=MyISAM

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=MyISAM

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=MyISAM

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=MyISAM

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=MyISAM

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id)

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=MyISAM

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=MyISAM

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=MyISAM

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=MyISAM

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=MyISAM

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=MyISAM

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id)

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=MyISAM

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=MyISAM

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=MyISAM

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=MyISAM

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=MyISAM

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=MyISAM

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id)

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=MyISAM

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=MyISAM

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=MyISAM

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=MyISAM

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=MyISAM

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=MyISAM

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id)

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=MyISAM

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=MyISAM

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=MyISAM

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=MyISAM

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=MyISAM

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=MyISAM

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=MyISAM

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id)

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id)

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id)

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id)

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        test_type varchar(255),
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table patient (
       id bigint not null auto_increment,
        age integer not null,
        phone_number varchar(255),
        sex varchar(255),
        user_id bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        record_date date,
        doctor_id bigint not null,
        patient_id bigint not null,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        seen integer,
        labexam_id bigint not null,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        email varchar(255),
        user_fname varchar(255),
        user_lname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table patient 
       add constraint UK_6i3fp8wcdxk473941mbcvdao4 unique (user_id)

    alter table result 
       add constraint UK_25ndghqcp9j5lu9nt3v4pkm2k unique (labexam_id)

    alter table appointment 
       add constraint FK3fl4y6087sfpvjbk1obgp5lrp 
       foreign key (doctor_id) 
       references user_data (id)

    alter table appointment 
       add constraint FK4apif2ewfyf14077ichee8g06 
       foreign key (patient_id) 
       references patient (id)

    alter table lab_exam 
       add constraint FKge9rpe2c2vupur107gdt9iuwh 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table lab_exam 
       add constraint FKq5uiit69rbkqadarg9jiiatlx 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table patient 
       add constraint FKdvgccn8fua1ste20t626lfa6q 
       foreign key (user_id) 
       references user_data (id)

    alter table record 
       add constraint FKmqagr683e5iwte6427cydxiji 
       foreign key (doctor_id) 
       references user_data (id) 
       on delete cascade

    alter table record 
       add constraint FKfv7pwtoln90pqf2luckkbg8cb 
       foreign key (patient_id) 
       references patient (id) 
       on delete cascade

    alter table result 
       add constraint FK1olcb4khpslghcxd9bohub8jb 
       foreign key (labexam_id) 
       references lab_exam (lab_exam_id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)
